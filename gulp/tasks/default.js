import gulp from 'gulp';
import runSequence from 'run-sequence';

gulp.task('default', () => {
	runSequence(
		'templates',
		'styles',
		'browserSync',
		'watch'
		);
});

gulp.task('build', ['del'], function() {
	runSequence(
		'styles',
		'templates',
		'copy'
	);
});

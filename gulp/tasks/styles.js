import gulp from 'gulp';
import sass from 'gulp-sass';
import browserSync from 'browser-sync';
import plumber from 'gulp-plumber';
import csscomb from 'gulp-csscomb';
import errorHandler from '../errorHandler';

gulp.task('styles', () => {
	return gulp
		.src('app/styles/*.sass')
		.pipe(plumber({errorHandler}))
		.pipe(sass())
		.pipe(csscomb())
		.pipe(gulp.dest('dist/css'))
		.pipe(browserSync.stream());
});

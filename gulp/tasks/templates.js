import gulp from 'gulp';
import include from 'gulp-file-include';
import plumber from 'gulp-plumber';
import errorHandler from '../errorHandler';
import browserSync from 'browser-sync';

gulp.task('templates', () => {
	return gulp
		.src(['app/templates/index.html', 'app/templates/blocks/content.html'])
		.pipe(include())
		.pipe(plumber({errorHandler}))
		.pipe(gulp.dest('dist'))
		.pipe(browserSync.stream());
});

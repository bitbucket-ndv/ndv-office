import gulp from 'gulp';
import browserSync from 'browser-sync';

gulp.task('scripts', ['copy:js'], () => {
	browserSync.reload('*.js');
});

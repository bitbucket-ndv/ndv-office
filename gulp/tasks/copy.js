import gulp from 'gulp';
import imagemin from 'gulp-imagemin';

gulp.task('copy:js', () => {
	gulp
		.src('app/scripts/*')
		.pipe(gulp.dest('dist/jscripts'));
});

gulp.task('copy:images', () => {
	gulp
		.src('app/images/*.{png,jpg,gif}')
		.pipe(imagemin({
			optimizationLevel: 3,
		}))
		.pipe(gulp.dest('dist/pic/design'));
})

gulp.task('copy', ['copy:images', 'copy:js']);

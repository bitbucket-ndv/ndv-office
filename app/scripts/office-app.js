$(window).ready(function() {

	function loadJS(src, cb) {
		var ref = window.document.getElementsByTagName( 'script' )[0];
		var script = window.document.createElement( 'script' );
		script.src = src;
		script.async = true;
		ref.parentNode.insertBefore(script, ref);
		if (cb && typeof(cb) === 'function') {
			script.onload = cb;
		}

		return script;
	}

	loadJS('https://api-maps.yandex.ru/2.1/?lang=ru_RU', function() {
		ymaps.ready(onLoadMap);
	});

	function onLoadMap() {
		var map = new ymaps.Map('map-popup', {
			center: [55.76, 37.64],
			zoom: 10,
			controls: [],
		});
		map.setType('yandex#hybrid');

		$('.link-map').each(function(i) {
			var $elm = $(this);
			$elm.attr('rel', 'mapbox-' + i).fancybox({
				onStart: function() {
					ymaps.geocode($elm.attr('data-address')).then(function(res) {
						map.geoObjects.removeAll();
						map.geoObjects.add(res.geoObjects.get(0));
					});
				}
			});
		});
	}

	$('.toggler__link').on('click', function(){
		var $parent = $(this).parents('.toggler').eq(0);
		$parent.toggleClass('is-close');
	});

	$('.slider').each(function(i) {
		$(this)
			.addClass('jCarouselLite-' + i)
			.find('.slider__thumbs').jCarouselLite({
				btnNext: '.jCarouselLite-' + i + ' .next',
				btnPrev: '.jCarouselLite-' + i + ' .prev',
				circular: false,
				visible: 3,
			});
	});

	/*
		Чтобы не было общего fancybox'а, для отдельных секций с ссылками указываем разные атрибуты `rel`
	 */
	$('.office-table__grid, .slider').each(function(i) {
		$(this)
			.find('a')
			.attr('rel', 'fancybox-' + i)
			.fancybox();
	});

});
